class Bike {
    constructor(data) {
        Object.assign(this, data);
    }

    getDescHtml() {
        return this.desc.map(function(paragraph) {
            return `<p class="item-desc-text">${paragraph}</p>`
        }).join('');
    }

    getCharListHtml() {
        return this.chars.map(function(char) {
            return `<li class="chars-item">${char}</li>`;
        }).join('');
    }

    getBikeHtml() {
        const { id, type, fullName, imgDir } = this;
    
        const descHtml = this.getDescHtml();
        const charListHtml = this.getCharListHtml();
    
        const bikeHtml =
            `
            <div class="tab">
                <button class="tab-btn btn-active" id="tab-btn-${id}">
                ${type}
                <img src="images/caret.png" class='caret rotated' id='caret-${id}'>
                </button>
            </div>
    
            <div class="container" id="item-${id}">
                <main>
                    <img class="main-img" src="${imgDir}">
                    <div class="item-desc">
                        <h1 class="bike-full-name">${fullName}</h1>
                        ${descHtml}
                    </div>
                </main>
    
                <section class="characteristics">
                    <h3>Характеристики</h3>
                    <ol class="chars-list">
                        ${charListHtml}
                    </ol>
                    <form class="add-form" id="add-form-${id}">
                        <h3>Добавить характеристику</h3>
                        <input class="add-input" id="add-input-${id}" required>
                        <button class="add-btn" data-add-btn="${id}">Добавить</button>
                    </form>
                    <div class="confirm-btns">
                        <button class="ok-btn">Окей</button>
                        <button class="cancel-btn">Отмена</button>
                    </div>
                </section>
    
            </div>
            `;
    
        return bikeHtml;
    }
}

export default Bike;