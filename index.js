import { bikeData } from "./data.js";
import Bike from './Bike.js';

document.addEventListener('click', function(e){
    if (e.target.id.startsWith('tab-btn')) {
        toggleItem(e.target.id)
    }
    else if(e.target.dataset.addBtn) {
        e.preventDefault();
        addChar(e.target.dataset.addBtn);
    }
})

function addChar(id) {
    const addForm = document.getElementById(`add-form-${id}`);
    const isValid = addForm.checkValidity();
    const addInput = document.getElementById(`add-input-${id}`);
    if (isValid) {
        let ol = document.getElementById(`item-${id}`).getElementsByTagName('ol')[0];
        let li = document.createElement('li');
        li.classList.add("chars-item");
        li.innerText = addInput.value;
        ol.appendChild(li);
        addInput.value = '';
    }
    else {
        addForm.reportValidity();
    }
}

function toggleItem(tabBtnId) {
    const id = Number(tabBtnId.replace('tab-btn-', ''))
    // true если начинаем отображать товар, false если скрываем.
    if (document.getElementById(`item-${id}`).classList.toggle('hidden')) {
        document.getElementById(tabBtnId).classList.remove('btn-active');
        document.getElementById(`caret-${id}`).classList.add('rotated');
    }
    else {
        document.getElementById(tabBtnId).classList.add('btn-active');
        document.getElementById(`caret-${id}`).classList.remove('rotated');
    }
}

function renderBody() {
    const body = document.getElementsByTagName('body')[0];    
    
    body.innerHTML = bikeData.map(function(bikeInfo) {
        const bike = new Bike(bikeInfo);
        return bike.getBikeHtml();
    }).join('');
}

renderBody();